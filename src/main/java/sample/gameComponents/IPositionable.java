package sample.gameComponents;


import java.awt.geom.Point2D;

/**
 * Created by jacko on 03.01.2017.
 */
public interface IPositionable {
    Point2D.Double getPosition();
}
