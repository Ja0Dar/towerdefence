package sample.gameComponents;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import sample.Point2DTools;

import java.awt.geom.Point2D;
import java.io.Serializable;

/**
 * Created by jacko on 29.12.2016.
 */
public class PathToProtect implements IDrawable, Serializable {
    private Point2D.Double[] points;


    public PathToProtect(Point2D.Double... points) {


        if (points.length < 2) {
            throw new IllegalArgumentException("There should be at least 2 points in path.");
        }

        this.points = points;
    }


    public Point2D.Double getPosOnPath(double mileage) {
        int i = 0;
        while (i < points.length - 1 && mileage > points[i].distance(points[i + 1])) {
            mileage -= points[i].distance(points[i + 1]);
            i++;
        }

        if (i >= (points.length - 1) && mileage > 0) {
            throw new IllegalArgumentException("mileage too big for this path");
        }

        return Point2DTools.pointInDirection(points[i], points[i + 1], mileage);

    }

    public double length() {
        double sum = 0;
        for (int i = 0; i < points.length - 1; i++) {
            sum += points[i].distance(points[i + 1]);
        }

        return sum;
    }


    @Override
    public void draw(GraphicsContext gc) {
        for (int i = 0; i < points.length - 1; i++) {
            gc.strokeLine(points[i].getX(), points[i].getY(), points[i + 1].getX(), points[i + 1].getY());
        }
        gc.setStroke(Color.RED);
        Point2D.Double p = points[points.length - 1];

        gc.strokeOval(p.getX() - 5, p.getY() - 5, 10, 10);
    }
}
