package sample.gameComponents;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by jacko on 30.12.2016.
 */
@Getter
@Setter

public class GameStats implements Serializable {

    private int lives;
    private int money;
    private int moneyGoal;

    public GameStats() {

    }

    public GameStats(int lives, int money, int moneyGoal) {
        if (lives <= 0 || money <= 0) {
            throw new IllegalArgumentException("Money and lives cannot be <0");
        }

        if (moneyGoal <= money) {
            throw new IllegalArgumentException("moneyGoal cannot be <= money that you have on start");
        }

        this.moneyGoal = moneyGoal;
        this.lives = lives;
        this.money = money;
    }

    public boolean gameEnded() {
        return gameWon() || gameOver();
    }

    public boolean gameWon() {
        return money > moneyGoal;
    }

    public boolean gameOver() {
        return lives <= 0;
    }


    public boolean canSpendMoney(int amount) {
        return money >= amount;
    }

    public void spendMoney(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("If you want to spend money amount must be >0");
        }

        if (!canSpendMoney(amount)) {
            throw new IllegalArgumentException("Cannot spend more money than you have. amount should be <= money");
        }
        this.money -= amount;

    }

    public void addMoney(int amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("If you want to add money amount must be >0");
        }
        money += amount;
    }

    public void takeHit(int livesTaken) {
        if (livesTaken <= 0) {
            throw new IllegalArgumentException("lives :livesTaken must be >0");
        }
        lives -= livesTaken;

    }


}
