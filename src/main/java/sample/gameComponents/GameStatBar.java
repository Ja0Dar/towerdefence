package sample.gameComponents;

import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import lombok.Getter;
import sample.IUpdatable;
import sample.Settings;

import java.io.Serializable;

/**
 * Created by jacko on 29.12.2016.
 */
public class GameStatBar implements IUpdatable, Serializable {
    private final int firstRemovableIndex;
    @Getter
    private GameStats gameStats;
    private HBox g;
    private Label moneyLabel, topLabel, goalLabel;

    public GameStatBar(GameStats gameStats, HBox g) {
        this.gameStats = gameStats;
        this.g = g;
        g.setSpacing(3);

        topLabel = new Label("Press P \n to pause");
        topLabel.setStyle(
                "    -fx-text-fill: rgb(49, 89, 23);\n" +
                        "    -fx-border-color: rgb(49, 89, 23);\n" +
                        "    -fx-border-radius: 5;\n" +
                        "    -fx-padding: 3 6 6 6;");

        moneyLabel = new Label();
        moneyLabel.setStyle(topLabel.getStyle());
        goalLabel = new Label("Goal: \n" + ((Integer) gameStats.getMoneyGoal()).toString() + "$");
        goalLabel.setStyle(topLabel.getStyle());


        this.g.getChildren().addAll(topLabel, moneyLabel, goalLabel);
        firstRemovableIndex = g.getChildren().size();
//        g.g
    }

    @Override
    public void update() {
        while (g.getChildren().size() - firstRemovableIndex > gameStats.getLives()) {
            g.getChildren().remove(firstRemovableIndex);
        }

        ImageView img;
        while (g.getChildren().size() - firstRemovableIndex < gameStats.getLives()) {
            img = new ImageView("heart.png");
            img.setFitHeight(Settings.HEIGHT / 30);
            img.setFitWidth(Settings.HEIGHT / 30);
            g.getChildren().add(img);
        }

        moneyLabel.setText("Money: \n" + ((Integer) this.gameStats.getMoney()).toString() + "$");


    }

}
