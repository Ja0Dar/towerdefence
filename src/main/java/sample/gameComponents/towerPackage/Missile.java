package sample.gameComponents.towerPackage;

import lombok.Getter;
import lombok.Setter;
import sample.IUpdatable;
import sample.gameComponents.IDrawable;
import sample.gameComponents.foePackage.Foe;

import java.io.Serializable;

/**
 * Created by jacko on 03.01.2017.
 */
@Getter
@Setter
public abstract class Missile implements IDrawable, IUpdatable, Serializable {
    public TowerBase sourceTowerBase;
    public Foe destination;
    private int speed;
    private int duration;
    private int damage;
    private boolean damageHasBeenDealt;
    public Missile(TowerBase sourceTowerBase, Foe destination, int damage) {
        this.sourceTowerBase = sourceTowerBase;
        this.destination = destination;
        this.damage = damage;
        damageHasBeenDealt = false;
    }

    public abstract boolean arrived();

    public void dealDamage() {
        if (damageHasBeenDealt) {
            throw new IllegalStateException("Missile cannot deal damage two times.");
        }
        destination.changeHp((short) (-1 * damage));
        damageHasBeenDealt = true;

    }

    public boolean hasDestination() {
        return !destination.isKilled();
    }
}
