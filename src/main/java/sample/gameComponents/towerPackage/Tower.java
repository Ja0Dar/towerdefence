package sample.gameComponents.towerPackage;

import javafx.scene.paint.Color;
import lombok.Getter;
import lombok.Setter;
import sample.gameComponents.foePackage.Foe;
import sample.gameComponents.foePackage.FoeGroup;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jacko on 29.12.2016.
 */


public abstract class Tower implements Serializable {
//    @Getter
//    private List<FoeGroup> foeGroups;
    @Getter
    @Setter
    private double range;
    @Getter
    private TowerBase towerBase;
    @Getter
    @Setter
    private double firingRate; // szybkość

    public Tower(TowerBase tc, double firingRate) {
//        this.foeGroups = foeGroups;
        this.towerBase = tc;
        if (towerBase == null ) {
            throw new IllegalArgumentException("Tower - towerBase and foeGroup can't be null.");
        }
        if (firingRate < 0.1 || firingRate > 100) {
            throw new IllegalArgumentException("Tower - firingRate has to be >0.1 and <100");
        }
        this.firingRate = firingRate;
    }

    public abstract Color getColor();

    public abstract List<Missile> getFiredMissiles();

    public boolean isInRange(Foe foe) {
        return foe.getPosition().distance(towerBase.getPosition()) < range;

    }


}

