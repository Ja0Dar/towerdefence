package sample.gameComponents.towerPackage;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import lombok.Getter;
import lombok.Setter;
import sample.*;
import sample.gameComponents.IClickable;
import sample.gameComponents.IDrawable;
import sample.gameComponents.IPositionable;
import sample.gameComponents.foePackage.FoeGroup;
import sample.gameComponents.GameStats;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by owner on 03.01.17.
 */
public class TowerBase implements IClickable, IDrawable, IUpdatable, IPositionable, Serializable {
    private Tower tower;
    private int level;
    private double x;
    private double y;
    @Getter
    @Setter
    private boolean hovered = false;
    @Getter
    private List<FoeGroup> foeGroups;
    private GameStats gameStats;
    private List<Missile> missileList;

    private int updatesBetweenShootingMissiles = 0;

    public TowerBase(double x, double y, List<FoeGroup> foeGroups, GameStats gameStats) {
        this.x = x - Settings.TOWERSIZE / 2;// zeby arg były srodki
        this.y = y - Settings.TOWERSIZE / 2;

        this.foeGroups = foeGroups;
        this.gameStats = gameStats;
        tower = null;
        level = 0;
        missileList = new ArrayList<>();
    }

    public int getUpgradeCost() {
        if (level == 3) {
            return Integer.MAX_VALUE;
        }
        return (level + 1) * 100;
    }

    public void buyTowerUpgrade() {
        if (!gameStats.canSpendMoney(getUpgradeCost())) {
            return;
        }

        gameStats.spendMoney(getUpgradeCost());
        level += 1;
        switch (level) {
            case 1:
                tower = TowerFactory.getFirstTower(this);
                break;
            case 2:
                tower = TowerFactory.getSecondTower(this);
                break;
            case 3:
                tower = TowerFactory.getThirdTower(this);

        }

    }

    @Override
    public void update() {
        if (tower == null) {
            return;
        }
        if (updatesBetweenShootingMissiles > 100 / tower.getFiringRate()) {

            missileList.addAll(tower.getFiredMissiles());
            updatesBetweenShootingMissiles = 0;
        } else {
            updatesBetweenShootingMissiles += 1;
        }
        missileList = missileList.stream().filter(missile -> (!missile.isDamageHasBeenDealt()) && missile.hasDestination()).collect(Collectors.toList());
        for (Missile missile : missileList
                ) {
            missile.update();

        }

    }

    @Override
    public void onClick() {
        buyTowerUpgrade();
    }

    @Override
    public void draw(GraphicsContext gc) {

        //Draw Range of tower
        if (hovered) {
            double radiusOld, radiusNew, cX, cY;//center X, center Y
            radiusOld = TowerFactory.getRange(level);
            radiusNew = TowerFactory.getRange(level + 1);
            cX = x + Settings.TOWERSIZE / 2;
            cY = y + Settings.TOWERSIZE / 2;

            //draw range of new tower

            gc.setFill(new Color(0, 1, 0, 0.2));
            gc.fillOval(cX - radiusNew, cY - radiusNew, 2 * radiusNew, 2 * radiusNew);// *2 because this is width, not radius


            gc.setFill(new Color(0, 1, 0, 0.5));
            gc.fillOval(cX - radiusOld, cY - radiusOld, radiusOld * 2, 2 * radiusOld);

            String message = (getUpgradeCost() < Integer.MAX_VALUE) ? ((Integer) getUpgradeCost()).toString() : "MAX";
            gc.setFill(Color.GOLD);
            gc.setFont(new Font(10));
            gc.fillText(message, x, y - 0.1 * Settings.TOWERSIZE);

        }

        ///DrawTower
        if (tower == null) {
            gc.setFill(Color.WHEAT);
        } else {

            gc.setFill(tower.getColor());
        }
        gc.fillRect(x, y, Settings.TOWERSIZE, Settings.TOWERSIZE);
        //Draw missiles

        for (Missile missile : missileList
                ) {
            missile.draw(gc);

        }


    }

    @Override
    public boolean containsPoint(Point2D.Double p) {
        return x <= p.getX() && p.getX() <= x + Settings.TOWERSIZE && y <= p.getY() && p.getY() <= y + Settings.TOWERSIZE;
    }

    @Override
    public Point2D.Double getPosition() {
        return new Point2D.Double(x + Settings.TOWERSIZE / 2, y + Settings.TOWERSIZE / 2);
    }
}
