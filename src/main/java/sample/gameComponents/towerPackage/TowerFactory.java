package sample.gameComponents.towerPackage;

import javafx.scene.paint.Color;
import sample.gameComponents.foePackage.Foe;
import sample.gameComponents.foePackage.FoeGroup;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.Integer.max;


/**
 * Created by jacko on 01.01.2017.
 */
public class TowerFactory {
    private static Integer[] ranges = new Integer[]{0, 100, 180, 250};// firsTowerRange =100, secondTowerRange =250, thirdTowerRange =300;


    public static double getRange(int level) {
        if (level < 0 || level >= ranges.length) {
            return 0;
        }
        return ((double) ranges[level]);
    }

    public static Tower getFirstTower(TowerBase tc) {
        return new Tower(tc, 10) {
            {
                setRange(ranges[1]);
            }

            @Override
            public Color getColor() {
                return Color.AQUA;
            }

            @Override
            public List<Missile> getFiredMissiles() {
                List<Missile> missiles = new ArrayList<>();
                Optional<Foe> dest = Optional.empty(), localDest;

                for (FoeGroup foeGroup : getTowerBase().getFoeGroups()) {
                    localDest = foeGroup.getFoes().stream().filter(foe -> isInRange(foe)).max(Comparator.comparing(Foe::getMileage));
                    if (dest.equals(Optional.empty())) {
                        dest = localDest;
                    } else if (!localDest.equals(Optional.empty())) {
                        if (localDest.get().getMileage() > dest.get().getMileage()) {
                            dest = localDest;
                        }
                    }
                }
                if (!dest.equals(Optional.empty())) {
                    missiles.add(MissileFactory.getBasicMissle(getTowerBase(), dest.get()));
                }
                return missiles;

            }


        };
    }

    public static Tower getSecondTower(TowerBase tc) {
        return new Tower(tc, 5) {
            {
                setRange(ranges[2]);
            }

            @Override
            public Color getColor() {
                return Color.DARKGOLDENROD;
            }

            @Override
            public List<Missile> getFiredMissiles() {
                List<Missile> missiles = new ArrayList<>();
                Optional<Foe> dest = Optional.empty(), localDest;// new Foe(null),localDest;

                for (FoeGroup foeGroup : getTowerBase().getFoeGroups()) {
                    localDest = foeGroup.getFoes().stream().filter(foe -> isInRange(foe)).max(Comparator.comparing(Foe::getPercentOfPath));
                    if (dest.equals(Optional.empty())) {
                        dest = localDest;
                    } else if (!localDest.equals(Optional.empty())) {
                        if (localDest.get().getMileage() > dest.get().getMileage()) {
                            dest = localDest;
                        }
                    }
                }
                if (!dest.equals(Optional.empty())) {
                    missiles.add(MissileFactory.getElectricityMissile(getTowerBase(), dest.get()));
                }
                return missiles;

            }


        };
    }


    public static Tower getThirdTower(TowerBase tc) {
        return new Tower(tc, 30) {
            {
                setRange(ranges[3]);
            }

            @Override
            public Color getColor() {
                return Color.AQUA;
            }

            @Override
            public List<Missile> getFiredMissiles() {
                List<Missile> missiles;

                List<Foe> allFoesInRange = getTowerBase().getFoeGroups().stream().map(FoeGroup::getFoes).flatMap(List::stream).filter(foe -> isInRange(foe)).collect(Collectors.toList());
                allFoesInRange.sort(Comparator.comparing(Foe::getPercentOfPath));

                missiles = allFoesInRange.subList(max(0, allFoesInRange.size() - 3), allFoesInRange.size()).stream()
                        .map(foe -> (Math.random() > 0.5) ? MissileFactory.getBasicMissle(getTowerBase(), foe) : MissileFactory.getElectricityMissile(getTowerBase(), foe))
                        .collect(Collectors.toList());
                return missiles;

            }


        };
    }


}
