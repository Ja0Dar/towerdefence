package sample.gameComponents.towerPackage;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import sample.Point2DTools;
import sample.gameComponents.foePackage.Foe;

import java.awt.geom.Point2D;

/**
 * Created by jacko on 03.01.2017.
 */
public class MissileFactory {
    public static Missile getBasicMissle(TowerBase source, Foe destination) {
        return new Missile(source, destination, 1) {
            private double percentageOfDistance;

            @Override
            public boolean arrived() {
                return percentageOfDistance >= 1;
            }

            @Override
            public void update() {
                if (arrived() && !isDamageHasBeenDealt() && hasDestination()) {//poor naming in getter :/
                    dealDamage();

                }
                percentageOfDistance += 0.1;
            }

            @Override
            public void draw(GraphicsContext gc) {
                if (arrived() || !hasDestination()) {
                    return;
                }
                Point2D.Double sourcePos = getSourceTowerBase().getPosition();
                Point2D.Double destPos = getDestination().getPosition();

                double size = 10;

                Point2D.Double misslePos = Point2DTools.pointBetween(sourcePos, destPos, percentageOfDistance);
                gc.setFill(Color.RED);
                gc.fillOval(misslePos.getX() - size / 2, misslePos.getY() - size / 2, size, size);

            }
        };
    }

    public static Missile getElectricityMissile(TowerBase source, Foe destination) {
        return new Missile(source, destination, 2) {
            private double timeToEnd = 1;

            @Override
            public boolean arrived() {
                return timeToEnd < 0;
            }

            @Override
            public void update() {
                if (arrived() && !isDamageHasBeenDealt() && hasDestination()) {//poor naming in getter :/
                    dealDamage();

                }
                timeToEnd -= 0.2;

            }

            @Override
            public void draw(GraphicsContext gc) {
                if (arrived() || !hasDestination()) {
                    return;
                }
                gc.setStroke(Color.BLUE);
                Point2D.Double f, t;
                f = destination.getPosition();
                t = source.getPosition();
                gc.strokeLine(t.getX(), t.getY(), f.getX(), f.getY());

            }
        };
    }
}
