package sample.gameComponents.foePackage;

import javafx.scene.canvas.GraphicsContext;
import lombok.Getter;
import sample.IUpdatable;
import sample.Settings;
import sample.gameComponents.GameStats;
import sample.gameComponents.IDrawable;
import sample.gameComponents.PathToProtect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jacko on 29.12.2016.
 */
public class FoeGroup implements IDrawable, IUpdatable, Serializable {
    private static final double eps = 0.1;
    private PathToProtect path;
    private int cyclesInBetweenSpawn;
    private GameStats gameStats;
    private int count;
    @Getter
    private List<Foe> foes;

    public FoeGroup(PathToProtect path, double timeBetweenSpawn, GameStats gameStats) {
        this.cyclesInBetweenSpawn = (int) (timeBetweenSpawn * Settings.FPS);
        this.path = path;
        this.gameStats = gameStats;
        this.foes = new ArrayList<>();
        this.count = 0;
    }

    @Override
    public void update() {
        for (Foe foe : foes) {
            foe.update();
        }

        if (cyclesInBetweenSpawn < count) {
            count = 0;

            int type = (int) (Math.random() * 9);
            if (type < 2) {
                foes.add(FoeFactory.getHeavyFoe(path));
            } else if (type < 6) {
                foes.add(FoeFactory.getFastFoe(path));
            } else {
                foes.add(FoeFactory.getBasicFoe(path));
            }

        } else {
            count += 1;
        }


        //Beautiful....
        List<Foe> foesOnPath = new ArrayList<>(), foesAtGates = new ArrayList<>(), foesKilled = new ArrayList<>();

        foes.forEach(foe -> ((foe.getMileage() + eps < path.length()) ? foesOnPath : foesAtGates).add(foe));
        foesAtGates.forEach(foe -> gameStats.takeHit(foe.getAttack()));

        foes = new ArrayList<>();
        foesOnPath.forEach(foe -> (foe.isKilled() ? foesKilled : foes).add(foe));

        foesKilled.forEach(foe -> gameStats.addMoney(foe.getMoneyForKill()));
    }

    @Override
    public void draw(GraphicsContext gc) {
        for (Foe foe : foes) {
            foe.draw(gc);
        }

    }
}



