package sample.gameComponents.foePackage;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import lombok.Getter;
import sample.IUpdatable;
import sample.Settings;
import sample.gameComponents.IClickable;
import sample.gameComponents.IDrawable;
import sample.gameComponents.IPositionable;
import sample.gameComponents.PathToProtect;

import java.awt.geom.Point2D;
import java.io.Serializable;

/**
 * Created by jacko on 30.12.2016.
 */

//TODO - make it abstract , Iclickable class
@Getter
public class Foe implements IDrawable, IUpdatable, IClickable, IPositionable, Serializable, Comparable {
    @Getter
    private int attack = 1;
    @Getter
    private int moneyForKill = 10;
    private int maxHP;
    private PathToProtect path;
    @Getter
    private double speed;
    @Getter
    private double mileage;
    private int hp;
    private double size;


    public Foe(PathToProtect path, int speed, int maxHP, int attack, double size, int moneyForKill) {
        if (speed<1|| attack<0|| size<0||moneyForKill<0||maxHP<0){
            throw new IllegalArgumentException("Foe- speed must be >0, attack,size,moneyForKill,maxHP : >=0");
        }
        this.path = path;
        this.speed = speed / Settings.FPS;
        this.attack = attack;
        this.size = size;
        this.moneyForKill = moneyForKill;
        this.mileage = 0;
        this.maxHP = maxHP;
        this.hp = maxHP;
    }

    public void changeHp(short hp) {
        this.hp += hp;
    }


    public boolean isKilled() {
        return this.hp <= 0 || this.mileage > path.length();
    }

    @Override
    public Point2D.Double getPosition() {
        return path.getPosOnPath(mileage);
    }

    @Override
    public void draw(GraphicsContext gc) {
        Point2D.Double p;
        double x, y;


        p = getPosition();
        x = p.getX() - size / 2;
        y = p.getY() - size / 2;

        gc.setFill(getColor());
        gc.fillOval(x, y, size, size);
        //draw health bar
        double greenWidth = hp * size / maxHP;
        double redWidth = size - greenWidth;
        gc.setFill(Color.GREEN);
        gc.fillRect(x, y - 0.2 * size, greenWidth, 3);

        gc.setFill(Color.RED);
        gc.fillRect(x + greenWidth, y - 0.2 * size, redWidth, 3);

    }

    @Override
    public void update() {
        mileage += speed;
    }


    @Override
    public void onClick() {
        changeHp((short) -1);
    }

    //Just for fun
    @Override
    public boolean containsPoint(Point2D.Double p) {
        double x, y;
        Point2D.Double mp = path.getPosOnPath(mileage);
        x = mp.getX() - size / 2;
        y = mp.getY() - size / 2;
        return x <= p.getX() && p.getX() <= x + size && y <= p.getY() && p.getY() <= y + size;
    }


    public Paint getColor() {
        //I do not store Colors beacause of serializable issues.
        return Color.RED;
    }

    public double getPercentOfPath() {
        return mileage / path.length();
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Foe) {
            if (this.getPercentOfPath() > ((Foe) o).getPercentOfPath()) {
                return 1;
            } else if (getPercentOfPath() == ((Foe) o).getPercentOfPath()) {
                return 0;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
}