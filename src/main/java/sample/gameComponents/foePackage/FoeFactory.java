package sample.gameComponents.foePackage;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import sample.gameComponents.PathToProtect;

/**
 * Created by owner on 05.01.17.
 */
public class FoeFactory {


    public static Foe getBasicFoe(PathToProtect pathToProtect) {
        return new Foe(pathToProtect, 200, 6, 1, 20, 10) {
            @Override
            public Paint getColor() {
                return Color.AQUA;
            }
        };
    }

    public static Foe getHeavyFoe(PathToProtect pathToProtect) {
        return new Foe(pathToProtect, 150, 12, 3, 23, 25) {
            @Override
            public Paint getColor() {
                return Color.ORCHID;
            }
        };
    }

    public static Foe getFastFoe(PathToProtect pathToProtect) {
        return new Foe(pathToProtect, 330, 4, 1, 17, 15) {
            @Override
            public Paint getColor() {
                return Color.PINK;
            }
        };
    }
}
