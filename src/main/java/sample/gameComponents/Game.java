package sample.gameComponents;


import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.Getter;
import sample.IUpdatable;
import sample.Settings;
import sample.gameComponents.foePackage.Foe;
import sample.gameComponents.foePackage.FoeGroup;
import sample.gameComponents.towerPackage.TowerBase;

import java.awt.geom.Point2D;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jacko on 29.12.2016.
 */
@Getter
public class Game implements IDrawable, IUpdatable, Serializable {
    private List<PathToProtect> paths;
    private List<FoeGroup> foeGroups;
    private List<TowerBase> towerBases;

//    private Canvas canvas;

    //Default constructor used in case of lack of maps
    public Game(GameStats gameStats, Canvas canvas) {
//        this.canvas = canvas;
        //Path
        Point2D.Double p1, p2, p3, p4;
        p1 = new Point2D.Double(50, 50);
        p2 = new Point2D.Double(10, 500);
        p3 = new Point2D.Double(700, 600);
        p4 = new Point2D.Double(400, 300);
        PathToProtect path = new PathToProtect(p1, p2, p3, p4);//hardcoded
        paths = Arrays.asList(new PathToProtect[]{path});

        //Foegroup
        FoeGroup foeGroup = new FoeGroup(path, Settings.SPAWNINTERVAL, gameStats);
        foeGroups = Arrays.asList(new FoeGroup[]{foeGroup});


        //Towers
        TowerBase t1, t2, t3;
        t1 = new TowerBase(600, 300, foeGroups, gameStats);
        t2 = new TowerBase(100, 100, foeGroups, gameStats);
        t3 = new TowerBase(100, 400, foeGroups, gameStats);

        towerBases = Arrays.asList(new TowerBase[]{t1, t2, t3});

        setUpCanvasClickListeners(canvas);
    }

    Game(GameStats gameStats, Canvas canvas, String mapName) throws IOException {
//        this.canvas = canvas;
        paths = new ArrayList<>();


//        File should look like:
//
//        =====
//        Cash
//        ====whatever
//        Cash Goal
//        ====sasa
//        Hearts
//        ====
//        towerX,TowerY
//        ===
//        path1pX,path1pY
//        path1pX,pah1pY
//        ===
//        path2pX,Path2pY
//        ....
//        ....
//        ====


        List<Point2D.Double> towerSpots = new ArrayList<Point2D.Double>();
        ArrayList<Point2D.Double> pathPoints = new ArrayList<Point2D.Double>();
        Point2D.Double[] pathPointsArr;
        String[] splitLine;


        List<String> lines= new ArrayList<>();
        String l;
//        lines = Files.readAllLines(Paths.get("src/main/resources/", mapName));
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(mapName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        while ((l=reader.readLine())!= null){
            lines.add(l);
        }

        int tmp;
        int counter = 0;
        double x, y;
        for (String line : lines
                ) {
            if (line.contains("//")) {
                continue;
            }
            System.out.println(line);
            if (line.contains("=")) {
                counter++;
                if (counter > 5) {// wiecej niż 1 path

                    pathPointsArr = pathPoints.toArray(new Point2D.Double[pathPoints.size()]);
                    paths.add(new PathToProtect(pathPointsArr));

                    pathPoints.clear();
                }
            } else {
                switch (counter) {
                    case 0://In case of lack of first "==="
                        throw new IllegalArgumentException("File is not a correct map file.");

                    case 1://InitialCash
                        tmp = Integer.parseInt(line);
                        if (tmp > 0) {
                            gameStats.setMoney(tmp);
                        } else {
                            throw new IllegalArgumentException("Initial cash in map file should be >0");
                        }
                        break;
                    case 2://MaxCash
                        tmp = Integer.parseInt(line);
                        if (tmp > 0) {
                            gameStats.setMoneyGoal(tmp);
                        } else {
                            throw new IllegalArgumentException("Cash goal in map file should be >0");
                        }
                    case 3://InitialHearts
                        tmp = Integer.parseInt(line);
                        if (tmp > 0) {
                            gameStats.setLives(tmp);

                        } else {
                            throw new IllegalArgumentException("Hearts in map file should be >0");
                        }
                        break;
                    case 4://Towers
                        splitLine = line.split(",");
                        x = Double.parseDouble(splitLine[0]);
                        y = Double.parseDouble(splitLine[1]);
                        towerSpots.add(new Point2D.Double(x, y));
                        break;
                    default://Paths
                        splitLine = line.split(",");
                        x = Double.parseDouble(splitLine[0]);
                        y = Double.parseDouble(splitLine[1]);
                        pathPoints.add(new Point2D.Double(x, y));
                        break;


                }
            }
        }
        foeGroups = paths.stream().map(path -> new FoeGroup(path, 0.4 + Math.random() * 0.3, gameStats)).collect(Collectors.toList());
        towerBases = towerSpots.stream().map(point -> new TowerBase(point.getX(), point.getY(), foeGroups, gameStats)).collect(Collectors.toList());
        setUpCanvasClickListeners(canvas);

    }

    Game(Canvas canvas, List<FoeGroup> foeGroups, List<TowerBase> towerBases, List<PathToProtect> paths) {// for serialization


//        this.canvas = canvas;
        this.foeGroups = foeGroups;
        this.towerBases = towerBases;
        this.paths = paths;
        setUpCanvasClickListeners(canvas);
    }

    private void setUpCanvasClickListeners(Canvas canvas) {
        canvas.setOnMouseClicked(e -> {
            Point2D.Double clickedPoint = new Point2D.Double(e.getX(), e.getY());
            for (TowerBase t : towerBases) {
                if (t.containsPoint(clickedPoint)) {
                    t.onClick();
                }
            }
            for (FoeGroup fG : foeGroups) {
                for (Foe foe : fG.getFoes()) {
                    if (foe.containsPoint(clickedPoint)) {
                        foe.onClick();
                    }
                }
            }

        });

        canvas.setOnMouseMoved(e ->
        {
            for (TowerBase tb : towerBases) {
                if (tb.isHovered()) {
                    if (!tb.containsPoint(new Point2D.Double(e.getX(), e.getY()))) {
                        tb.setHovered(false);
                    }

                } else {
                    if (tb.containsPoint(new Point2D.Double(e.getX(), e.getY()))) {
                        tb.setHovered(true);
                    }

                }
            }
        });
    }

    @Override
    public void draw(GraphicsContext gc) {
        Canvas c = gc.getCanvas();
        double h, w;
        h = c.getHeight();
        w = c.getWidth();
        gc.setFill(Color.BLACK);
        gc.fillRect(0, 0, w, h);


        for (TowerBase t : towerBases) {
            t.draw(gc);
        }

        for (PathToProtect path : paths) {
            gc.setStroke(Color.GREEN);
            path.draw(gc);
        }

        for (FoeGroup foeGroup : foeGroups) {
            foeGroup.draw(gc);
        }

    }

    @Override
    public void update() {


        for (FoeGroup foeGroup : foeGroups) {
            foeGroup.update();
        }
        for (TowerBase t : towerBases) {
            t.update();
        }

    }
}
