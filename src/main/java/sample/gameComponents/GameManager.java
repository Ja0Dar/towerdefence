package sample.gameComponents;

import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import lombok.Getter;
import sample.IUpdatable;
import sample.Settings;
import sample.gameComponents.foePackage.FoeGroup;
import sample.gameComponents.towerPackage.TowerBase;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Created by jacko on 29.12.2016.
 */
@Getter
public class GameManager implements IDrawable, IUpdatable, Serializable {

    private Game game;
    private GameStatBar gameStatBar;
    public GameManager(HBox topRow, Canvas canvas) {//Default
        GameStats gameStats = new GameStats(3, 500, 1000);
        game = new Game(gameStats, canvas);
        gameStatBar = new GameStatBar(gameStats, topRow);
    }


    public GameManager(HBox topRow, Canvas canvas, String mapName) throws IOException {
        GameStats gameStats = new GameStats(); // values inserted in Game class
        game = new Game(gameStats, canvas, mapName);//Hearts and Cash are overwritten in gamemap unless they are 0 in file
        gameStatBar = new GameStatBar(gameStats, topRow);
    }

    public GameManager(HBox topRow, Canvas canvas, GameStats stats, List<FoeGroup> foeGroups, List<TowerBase> towerBases, List<PathToProtect> paths) {//for serialization
        GameStats gameStats = stats;
        game = new Game(canvas, foeGroups, towerBases, paths);
        gameStatBar = new GameStatBar(gameStats, topRow);
    }

    @Override
    public void update() {
        if (!gameStatBar.getGameStats().gameEnded()) {
            game.update();
            gameStatBar.update();

        }

    }

    @Override
    public void draw(GraphicsContext gc) {
        if (gameStatBar.getGameStats().gameEnded()) {
            String message;
            gc.setStroke(Color.BLUE);
            gc.setTextBaseline(VPos.CENTER);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setFont(Font.font("Comic Sans", 60));
            if (gameStatBar.getGameStats().gameOver()) {
                gc.setFill(Color.RED);
                message = "GAME OVER";
            } else {//game Won
                gc.setFill(Color.SALMON);
                message = "GAME WON";
            }

            gc.strokeText(message, Settings.WIDTH / 2, Settings.HEIGHT * 0.3);
            gc.fillText(message, Settings.WIDTH / 2, Settings.HEIGHT * 0.3);
            gc.fillText("Press P for pause \n and Restart for playing again", Settings.WIDTH / 2, Settings.HEIGHT * 0.6);


        } else {
            game.draw(gc);
        }
    }
}