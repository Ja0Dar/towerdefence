package sample.gameComponents;


import java.awt.geom.Point2D;

/**
 * Created by jacko on 01.01.2017.
 */
public interface IClickable {
    void onClick();
    boolean containsPoint(Point2D.Double p);
}
