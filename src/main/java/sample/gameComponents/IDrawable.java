package sample.gameComponents;

import javafx.scene.canvas.GraphicsContext;

/**
 * Created by jacko on 29.12.2016.
 */
public interface IDrawable {
    void draw(GraphicsContext gc);
}
