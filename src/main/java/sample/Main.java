package sample;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.stage.Stage;
import sample.fxManagers.*;


public class Main extends Application {



    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        ISceneManager menuSM, gameSM, pauseSM;
        StageWrapper sw = new StageWrapper(primaryStage);
        primaryStage.setTitle("Tower Defence");
        primaryStage.setResizable(false);
        menuSM = new MenuSM(sw);
        gameSM = new GameSM(sw);
        pauseSM = new PauseSM(sw);
//        sw.setPauseSM(pauseSM);

        AnimationTimer timer = new AnimationTimer() {
            long oldTime = System.nanoTime();

            @Override
            public void handle(long now) {
                if (now - oldTime > 1000000000 / Settings.FPS) {
                    oldTime = now;

                    menuSM.update();
                    pauseSM.update();
                    gameSM.update();
                }
            }
        };
        timer.start();
        sw.setMenuScene();
        primaryStage.show();

    }

}
