package sample;

/**
 * Created by jacko on 30.12.2016.
 */
public interface IUpdatable {
    void update();
}
