package sample;

/**
 * Created by jacko on 30.12.2016.
 */
public class Settings {
    public static final int FPS = 60;
    public static final int TOWERSIZE = 20;
    public static final double WIDTH = 1000;
    public static final double HEIGHT = 720;
    public static final String SAVENAME = "plik";
    public static final double MAPHEIGHT = 0.95 * HEIGHT;
    public static final double SPAWNINTERVAL = 0.5;
    public static final String[] MAPS = new String[]{"map1", "map2", "map3"};
}
