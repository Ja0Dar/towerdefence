package sample.fxManagers;

import javafx.scene.Scene;
import sample.IUpdatable;

/**
 * Created by jacko on 29.12.2016.
 */
public interface ISceneManager extends IUpdatable {
    Scene getScene();

    boolean isActive();

    void activate();

    void deactivate();
}
