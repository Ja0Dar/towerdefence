package sample.fxManagers;

import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by jacko on 29.12.2016.
 */

@Getter
public class StageWrapper {
    @Setter
    private Stage stage;
    private AbstractSceneManager menuSM, gameSM, pauseSM;

    public StageWrapper(Stage s) {
        this.stage = s;

    }

    public void setGameScene() {
        if (gameSM == null) {
            throw new NullPointerException("Currently this scene manager is null.");
        }
        gameSM.activate();
        stage.setScene(gameSM.getScene());
        pauseSM.deactivate();
        menuSM.deactivate();
    }


    public void setMenuScene() {
        if (menuSM == null) {
            throw new NullPointerException("Currently this scene manager is null.");
        }
        menuSM.activate();
        stage.setScene(menuSM.getScene());
        gameSM.deactivate();
        pauseSM.deactivate();
    }


    public void setPauseScene() {
        if (pauseSM == null) {
            throw new NullPointerException("Currently this scene manager is null.");
        }
        pauseSM.activate();
        stage.setScene(pauseSM.getScene());
        gameSM.deactivate();
        menuSM.deactivate();
    }


    private void setStyles(AbstractSceneManager abstractSceneManager) {
        abstractSceneManager.getScene().getStylesheets().add("mainStyles.css");
    }


     void setMenuSM(AbstractSceneManager menuSM) {
        this.menuSM = menuSM;
        setStyles(menuSM);
    }

     void setGameSM(AbstractSceneManager gameSM) {
        this.gameSM = gameSM;
        setStyles(gameSM);
    }

     void setPauseSM(AbstractSceneManager pauseSM) {
        this.pauseSM = pauseSM;
        setStyles(pauseSM);
    }
}
