package sample.fxManagers;

import javafx.scene.Parent;
import javafx.scene.Scene;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by jacko on 29.12.2016.
 */
@Getter
@Setter
public abstract class AbstractSceneManager implements ISceneManager {
    private boolean active;
    private Scene scene;
    private Parent container;
    private StageWrapper sw;

    public AbstractSceneManager(StageWrapper sw) {
        this.sw = sw;
        this.scene = null;
        deactivate();

    }


    @Override
    public void activate() {
        active = true;
    }

    @Override
    public void deactivate() {
        active = false;
    }
}
