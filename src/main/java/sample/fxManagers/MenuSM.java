package sample.fxManagers;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import sample.*;

import java.io.IOException;

/**
 * Created by jacko on 29.12.2016.
 */

public class MenuSM extends AbstractSceneManager {
    private Button playB, loadB, exitB, newGameB;
    private Label titleLabel;
//    private StageWrapper sw;


    public MenuSM(StageWrapper sw) {
        super(sw);

        titleLabel = new Label("Tower Defence");
        titleLabel.setStyle("");
        titleLabel.setStyle("-fx-font-size: 90pt; -fx-font-family:Verdana;");
        titleLabel.setPadding(new Insets(0, 0, 150, 0));

        playB = new Button("Play");
        newGameB = new Button("New Game");
        loadB = new Button("Load");
        exitB = new Button("Exit");


        playB.setOnAction(e -> sw.setGameScene());
        newGameB.setOnAction(e -> {
            ((GameSM) sw.getGameSM()).resetGame();
            sw.setGameScene();
        });
        loadB.setOnAction(e -> {


            try {
                ((GameSM) sw.getGameSM()).loadGame();
                Toast.makeText(sw.getStage(), "Game Loaded", Color.GREEN, 4000, 300, 400);
            } catch (IOException e1) {
                Toast.makeText(sw.getStage(), "I couldn't find good save file.", 3000, 400, 400);
            } catch (ClassNotFoundException e1) {
//                e1.printStackTrace();
                Toast.makeText(sw.getStage(), "I couldn't find good save file.", 3000, 400, 400);
            }
        });
        exitB.setOnAction(e -> sw.getStage().close());

        setContainer(new VBox(30, titleLabel, playB, newGameB, loadB, exitB));

        ((VBox) getContainer()).setAlignment(Pos.CENTER);

        setScene(new Scene(getContainer(), Settings.WIDTH, Settings.HEIGHT));


        sw.setMenuSM(this);
    }


    @Override
    public void update() {
        if (!isActive()) {
            return;
        }
    }

}
