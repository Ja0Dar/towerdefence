package sample.fxManagers;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import sample.*;

import java.io.IOException;

/**
 * Created by owner on 05.01.17.
 */
public class PauseSM extends AbstractSceneManager {
    private Button mainMenuB, exitB, saveB, restartB;
    private Label topLabel;

    public PauseSM(StageWrapper sw) {
        super(sw);
        topLabel = new Label("Press P to return, R to restart");
        topLabel.setPadding(new Insets(150, 0, 0, 0));
        mainMenuB = new Button("Return to Menu");
        restartB = new Button("Restart");
        exitB = new Button("Exit");
        saveB = new Button("Save");


        setContainer(new VBox(30, topLabel, mainMenuB, restartB, saveB, exitB));
        ((VBox) getContainer()).setAlignment(Pos.CENTER);
        setScene(new Scene(getContainer(), Settings.WIDTH, Settings.HEIGHT));

        //Event listeners:
        mainMenuB.setOnAction(e -> sw.setMenuScene());
        restartB.setOnAction(e -> {
            ((GameSM) sw.getGameSM()).resetGame();
            sw.setGameScene();
        });
        saveB.setOnAction(e -> {

            try {
                ((GameSM) sw.getGameSM()).saveGame();
                Toast.makeText(sw.getStage(), "Game Saved", Color.GREEN, 1000, 300, 300);
            } catch (IOException e1) {
                Toast.makeText(sw.getStage(), "Couldn't save game", 1000, 300, 300);
            }
        });


        exitB.setOnAction(e -> sw.getStage().close());
        getScene().setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.P) {
                sw.setPauseSM(this);
                sw.setGameScene();
            } else if (e.getCode() == KeyCode.R) {
                ((GameSM) sw.getGameSM()).resetGame();
                sw.setGameScene();
            }
        });


        sw.setPauseSM(this);
    }

    @Override
    public void update() {
//        nothing
    }


}