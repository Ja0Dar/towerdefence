package sample.fxManagers;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import lombok.Getter;
import sample.Settings;
import sample.gameComponents.PathToProtect;
import sample.gameComponents.foePackage.FoeGroup;
import sample.gameComponents.GameManager;
import sample.gameComponents.GameStats;
import sample.gameComponents.towerPackage.TowerBase;

import java.io.*;
import java.util.List;

/**
 * Created by jacko on 29.12.2016.
 */
public class GameSM extends AbstractSceneManager {

    @Getter
    private Canvas canvas;
    @Getter
    private HBox topRow;
    @Getter
    private GameManager gameManager;

    private String[] availableMaps = Settings.MAPS;

    public GameSM(StageWrapper sw) {// is it reasonable to create abstract class implementing  ISceneManager?
        super(sw);
        topRow = new HBox();


        canvas = new Canvas(Settings.WIDTH, Settings.MAPHEIGHT);


        BorderPane bp = new BorderPane();
        bp.setTop(topRow);
        bp.setCenter(canvas);
        setContainer(bp);
        setScene(new Scene(getContainer(), Settings.WIDTH, Settings.HEIGHT));

        ///Pause on clicked P
        getScene().setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.P) {
                sw.setPauseScene();
            } else {
                sw.setGameScene();
            }
        });
        resetGame(); // and init it
        sw.setGameSM(this);

    }

    public void resetGame() {
        topRow.getChildren().clear();

        String mapName = availableMaps[(int) (Math.random() * availableMaps.length)];
        try {
            gameManager = new GameManager(topRow, canvas, mapName);
        } catch (IOException e) {
            System.err.println("Couldnt open map " + mapName+". Loading default map.");
            gameManager = new GameManager(topRow, canvas);
        }

    }

    @Override
    public void update() {
        if (!isActive()) {
            return;
        }
        gameManager.update();
        gameManager.draw(canvas.getGraphicsContext2D());

    }

    public void saveGame() throws IOException {

//      Game constructor:
//      public Game(Pane topRow, Canvas canvas, GameStats stats, List< FoeGroup > foeGroups, List< TowerBase > towerBases, List< PathToProtect > paths) {//for serialization
        FileOutputStream fos;
        ObjectOutputStream oos;
        fos = new FileOutputStream(Settings.SAVENAME);
        oos = new ObjectOutputStream(fos);
        oos.writeObject(gameManager.getGameStatBar().getGameStats());
        oos.writeObject(gameManager.getGame().getFoeGroups());
        oos.writeObject(gameManager.getGame().getTowerBases());
        oos.writeObject(gameManager.getGame().getPaths());
        oos.close();
    }

    public void loadGame() throws IOException, ClassNotFoundException {
        GameStats stats;
        List<FoeGroup> foeGroups;
        List<TowerBase> towerBases;
        List<PathToProtect> paths;

//      Game constructor:
//  public Game(Pane topRow, Canvas canvas, GameStats stats, List< FoeGroup > foeGroups, List< TowerBase > towerBases, List< PathToProtect > paths) {//for serialization
        FileInputStream fis;
        ObjectInputStream ois;

        fis = new FileInputStream(Settings.SAVENAME);
        ois = new ObjectInputStream(fis);

        stats = (GameStats) ois.readObject();
        foeGroups = (List<FoeGroup>) ois.readObject();
        towerBases = (List<TowerBase>) ois.readObject();
        paths = (List<PathToProtect>) ois.readObject();
        ois.close();
        fis.close();
        //Creating new game with loaded things
        topRow.getChildren().clear();
        gameManager = new GameManager(topRow, canvas, stats, foeGroups, towerBases, paths);


    }

}
