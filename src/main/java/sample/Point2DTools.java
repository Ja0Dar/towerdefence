package sample;


//


import java.awt.geom.Point2D;

/**
 * Created by jacko on 29.12.2016.
 */
public class Point2DTools {

    public static Point2D.Double pointInDirection(Point2D.Double start, Point2D.Double end, double distance) {
//        double phi= Math.atan(slope(start, end));

        double cosp, sinp, vx, vy, vlen;
        vlen = start.distance(end);

        vx = end.getX() - start.getX();
        vy = end.getY() - start.getY();

        // From scalar product with vector A=[1,0]
        // |A|*|V| * cos(A,V)= A.dx*V.dx+A.dy*V.dy
        //cos(A,V)=1*vx+0*vy  over 1*vlen

        cosp = vx / vlen; // damn, it is so obvious
        sinp = vy / vlen;
        float dx = (float) (cosp * distance);
        float dy = (float) (sinp * distance);
        return new Point2D.Double(start.getX() + dx, start.getY() + dy);

    }


    public static Point2D.Double pointBetween(Point2D.Double start, Point2D.Double end, double percentageOfDistance) {
        return pointInDirection(start, end, percentageOfDistance * start.distance(end));
    }

}
